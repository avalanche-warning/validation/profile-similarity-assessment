# NWP-model-analysis

A R notebook for assessing the similarity between simulated snow profiles (profile and AWS simulations) and a reference profile. Corresponding profile from the simulation (.pro-file) is automatically selected.

<p float="center">
  <img src="./figures/similarity_assessment_axliz.png" width="85%" />
</p>

<p float="center">
  <img src="./figures/profile-alignment-prof.png" width="45%" />
  <img src="./figures/cost-density-prof.png" width="45%" />
</p>